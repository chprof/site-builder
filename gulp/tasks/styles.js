module.exports = function () {
    var processors = [
        $.prefix({
            browsers: ['last 10 version'],
            cascade: false
        }),
        $.mqpc({
            sort: $.sortMedia
        })
    ]
    $.gulp.task("styles", function () {
        return $.gulp.src("./app/styles/main.scss")
            // .pipe($.cached())
            .pipe($.gp.plumber())
            // .pipe($.gp.sourcemaps.init())
            .pipe($.gp.sass({
                outputStyle: 'expanded',
                errLogToConsole: true
            }).on('error', $.gp.sass.logError))
            .pipe($.cssPost(processors))
            .pipe($.gp.replace("../../build/", "../"))
            .pipe($.gp.plumber.stop())
            // .pipe($.gp.sourcemaps.write("./"))
            .pipe($.gulp.dest("./build/styles/"))
            .pipe($.debug({ "title": "styles" }))
            .on("end", $.bs.reload);
    });
};