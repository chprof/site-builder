module.exports = function() {
    $.gulp.task("html", function() {
        return $.gulp.src(["./app/views/**/*.html", "!./app/views/blocks/*.html"])
        	.pipe($.cached())
            .pipe($.rigger())
            .pipe($.gp.replace("../build/", "../"))
            .pipe($.gulp.dest("./build/"))
            .pipe($.debug({"title": "html"}))
            .on("end", $.bs.reload);
    });
};