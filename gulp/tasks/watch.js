module.exports = function() {
    $.gulp.task("watch", function() {
        return new Promise((res, rej) => {
            $.gp.watch("./app/views/**/*.html", $.gulp.series("html"));
            $.gp.watch("./app/styles/**/*.scss", $.gulp.series("styles"));
            // $.gp.watch("./app/css/**/*.css", $.gulp.series("cssVendor"));
            // $.gp.watch("./app/fonts/**/*.{eot,woff,woff2,ttf,otf}", $.gulp.series("fonts"));
            // $.gp.watch("./app/images/**/*.{jpg,jpeg,png,gif,svg}", $.gulp.series("images"));
            // $.gp.watch("./app/icons/svg/*.svg", $.gulp.series("iconfont"));
            $.gp.watch("./app/js/**/*.js", $.gulp.series("scripts"));
            // $.gp.watch("./app/webfonts/**/*.{ttf,svg,eot,woff,woff2}", $.gulp.series("bowerFonts"));
            res();
        });
    });
};