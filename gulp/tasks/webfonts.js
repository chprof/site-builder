module.exports = function () {
    $.gulp.task("webfonts", function () {
        return $.gulp.src("./app/webfonts/**/*")
            .pipe($.gulp.dest("./build/webfonts/"))
            .pipe($.debug({ "title": "fonts" }))
            .on("end", $.bs.reload);
    });
};