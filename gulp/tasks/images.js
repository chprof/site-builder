module.exports = function() {
    $.gulp.task("images", function() {
        return $.gulp.src("./app/images/**/*.{jpg,jpeg,png,gif,svg}")
            // .pipe($.gp.wait(500))
            .pipe($.gp.newer("./app/images/**/*"))
            // .pipe($.gp.wait(500))
            // .pipe($.gp.imagemin([
            //     $.imagemin.gifsicle({interlaced: true}),
            //     $.imagemin.jpegtran({progressive: true}),
            //     $.imageminJpegRecompress({loops: 1, quality: "low"}),
            //     $.imagemin.svgo(),
            //     $.imagemin.optipng({optimizationLevel: 5}),
            //     $.pngquant({quality: "65-70", speed: 5})
            // ]))
            .pipe($.gulp.dest("./build/images/"))
            .pipe($.debug({"title": "images"}))
            .on("end", $.bs.reload);
    });
};