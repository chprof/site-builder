module.exports = function () {
    $.gulp.task('bowerFonts', function() {
		return $.gulp.src($.mainFiles('**/*.{woff,woff2,ttf,svg,eot}'))
		.pipe($.gulp.dest('./app/fonts/'))
	  	.pipe($.debug({ "title": "bowerFonts" }))
	  	.on("end", $.bs.reload);
	});
};