#For using this template you need bower and gulp
##for install gulp:
	1. npm i gulp -g
	2. npm i
##for install bower:
	1. npm bower i -g
	2. bower i (for install dependencies - jquery, boostrap...)

##After that you need launch gulp, enter to console command -
	1. gulp

##if problems will happened, enter to console -
	1. npm audit fix

##if gulp launched, you need use jquery libraries from bower
###Enter to console -
	1. gulp bower


## Check bower dependencies. .bower.json or bower.json in bower folder in main directory.
### for example bower/bootstrap/.bower.json you need add main array with direct path for css, js files
```javascript
	{
	"name": "bootstrap",
	"homepage": "https://github.com/twbs/bootstrap",
	"version": "4.2.1",
	"_release": "4.2.1",
	"_resolution": {
		"type": "version",
		"tag": "v4.2.1",
		"commit": "9e4e94747bd698f4f61d48ed54c9c6d4d199bd32"
	},
	"main": [
		"dist/css/bootstrap.css",
		"dist/js/bootstrap.js"
	],
	"_source": "https://github.com/twbs/bootstrap.git",
	"_target": "^4.2.1",
	"_originalSource": "bootstrap",
	"_direct": true
	}
```