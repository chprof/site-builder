(function() {
	var dStart = new Date(),
		doc = $(document),
		win = $(window),
		scrollT = win.scrollTop(),
		headerWithoutBackgound = $('.header_background_none'),
		dataAttr,
		header = $('[data-id="header"]'),
		footer = $('[data-id="footer"]'),
		loader = $('[data-id="loading"]'),
		docH, headerH, footerH, dEnd, totalD;

	var waitForFinalEvent = (function () {
		var timers = {};
		return function (callback, ms, uniqueId) {
			if (!uniqueId) {
				uniqueId = "Don't call this twice without a uniqueId";
			}
			if (timers[uniqueId]) {
				clearTimeout (timers[uniqueId]);
			}
			timers[uniqueId] = setTimeout(callback, ms);
		};
	})();
	function calcValue() {
		headerH = header.outerHeight();
		footerH = footer.outerHeight();
	};
	function categoryMenuOnScroll() {
		if ( $('[data-id="fixedOnScroll"]').length ) {
			$('[data-id="aside-category"]').css({'padding-top': headerH, 'padding-bottom': '0'});
			$('.section').addClass('section_has_sibling_aside');
			if ( scrollT > 0 ) {
				$('[data-id="aside-category"]').css({'padding-top': '0', 'padding-bottom': '0'});
			}
			if ( scrollT >= doc.height() - win.height() - footerH ) {
				$('[data-id="aside-category"]').css({'padding-bottom': footerH});
			}
		}
	}
	function clearClasses() {
		if ( win.width() > (768-18) ) {
			$('body').removeClass('scroll-hidden');
		}
	};
	function clearClassesOnResize() {
		if ( win.width() > (768-18) ) {
		}
	}
	function mobileFullPageToggle() {
		$('[data-open]').click(function() {
			dataAttr = $(this).attr('data-open');
			$('body').addClass('scroll-hidden')
			$('[data-id="' + dataAttr + '"]').addClass('visible show');
		})
		$('[data-close]').click(function() {
			dataAttr = $(this).attr('data-close');
			$('body').removeClass('scroll-hidden');
			$('[data-id="' + dataAttr + '"]').removeClass('show visible');
			$('[data-toggle="collapse"]').addClass('collapsed')
		})
	};
	function calcScale() {
		var scale = $('[data-scale-id="scale"]');
		scale.each(function() {
			var total = $(this).attr('data-scale-total'),
				current = $(this).attr('data-scale-current'),
				descrCur = $(this).parents('.percent-block').find('[data-scale-descr-current]'),
				descrTotal = $(this).parents('.percent-block').find('[data-scale-descr-total]'),
				descrCurpercent = $(this).parents('.percent-block').find('[data-scale-descr-curr-percent]'),
				calcedInPercent = (100 - (current/total * 100) ).toFixed(1);
			$(this).width( calcedInPercent + '%' );
			descrCur.text( current + 'Gb' );
			descrTotal.text( total + 'Gb' );
			descrCurpercent.text( '('+ (100 - calcedInPercent) + '%' +')' );
		})
	};
	function removeElemFormDoom() {
		$('[data-remove-target]').click(function(e) {
			e.preventDefault();
			var target = $(this).attr('data-remove-target');
			$('[data-id="'+ target +'"]').remove();
		})
	};
	function setScroll() {
		if ( win.width() >= (768-17) ) {
			$('[data-scroll="axisY"]').mCustomScrollbar({
				axis:"y",
				scrollEasing:"linear",
				scrollInertia:100,
				timeout:500
			});
		}
		if ( win.width() < (768-17) ) {
			$('[data-scroll="axisY"]').mCustomScrollbar("destroy");
		}
	}
	function setCheckbox() {
		$('[data-element="checkbox"]').change(function() {
			$(this).closest('tr').toggleClass('hasSettings')
		})
	}
	win.on('load resize scroll', function(e) {
		if ( e.type == 'load' ) {
			dEnd = new Date();
			totalD = dEnd - dStart;
			setTimeout( function() {
				loader.fadeOut();
			}, totalD);
		}
		if ( e.type == 'resize' ) {
			waitForFinalEvent(function() {
				calcValue();
				clearClasses();
				clearClassesOnResize();
				setScroll();
			}, 500, "resize")
		}
		if ( e.type == 'scroll' ) {
			scrollT = $(this).scrollTop();
			calcValue();
			categoryMenuOnScroll();
			if ( scrollT > 0 && !headerWithoutBackgound.hasClass('stick') ) {
				headerWithoutBackgound.addClass('stick')
			} else if ( scrollT == 0 ) {
				headerWithoutBackgound.removeClass('stick')
			}
		}
	})

	doc.on('click', function(e) {
		if ( e.type == 'click' ) {
			var target = $(e.target);
		}
	})
	doc.ready(function() {
		calcValue();
		setScroll();
		calcScale();
		categoryMenuOnScroll();
		svg4everybody();
		clearClasses();
		mobileFullPageToggle();
		removeElemFormDoom();
		setCheckbox();
		$('.select-custom').selectize({
			create: true,
			sortField: 'text'
		});
		$('[data-select="selectize-tags"]').selectize({
			plugins: ['remove_button'],
			delimiter: ',',
			persist: false,
			create: function(input) {
				return {
					value: input,
					text: input
				}
			}
		});
		$('[date-picker="date-time-24h"]').datetimepicker({
			format: 'dddd, MMMM Do YYYY, HH:mm',
			debug: true
		});
		$('[data-picker="date"]').datetimepicker({
			format: 'L'
		});
		$('[data-picker="24hformat"]').datetimepicker({
			format: 'HH:mm'
		});
		if ( $(window).width() == 394 ) {
			document.write( $(window).width() )
		} else if ( $(window).width() == 787 ) {
			document.write( $(window).width() )
		}
		console.log( win.width() )
	})
}())